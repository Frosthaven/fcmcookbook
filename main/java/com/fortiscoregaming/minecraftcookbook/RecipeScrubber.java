package com.fortiscoregaming.minecraftcookbook;

import com.fortiscoregaming.minecraftcookbook.fluid.CustomFluids;
import com.fortiscoregaming.minecraftcookbook.tconstruct.InitSmeltery;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistryModifiable;

@Mod.EventBusSubscriber
public class RecipeScrubber {
	public static String[] recipeBlacklist = {
			"tconstruct:clear_glass",
			"projectred-core:resource/red_silicon_compound",
			"projectred-core:resource/glowing_silicon_compound",
			"projectred-core:resource/electrotine_silicon_compound",
			"icbmclassic:tools/radargun",
			"icbmclassic:machine/emptower",
			"icbmclassic:machine/radar",
			"icbmclassic:launcher/base/base.tier.1",
			"icbmclassic:launcher/base/base.tier.2",
			"icbmclassic:launcher/base/base.tier.3",
			"icbmclassic:launcher/screen/screen.tier.1",
			"icbmclassic:launcher/screen/screen.tier.2",
			"icbmclassic:launcher/screen/screen.tier.3",
			"icbmclassic:explosives/incendiary",
			"icbmclassic:explosives/debilitation",
			"icbmclassic:tools/rpg",
			"icbmclassic:tools/tracker",
			"icbmclassic:tools/defuser",
			"icbmclassic:tools/remote",
			"icbmclassic:tools/laser",
			"xnet:netcable_0",
			"xnet:netcable_routing",
			"quark:world_stone_bricks",
			"quark:stone_marble_bricks_stairs",
			"quark:stone_marble_bricks_slab",
			"quark:stone_marble_bricks_wall",
			"quark:marble",
			"quark:stone_marble_slab",
			"quark:stone_marble_tairs",
			"quark:marble_wall"
	};

	/*
	public static String[] blockBlacklist = {
			"tconstruct:clear_glass",
			"tconstruct:clear_stained_glass"
	};

	public static String[] itemBlacklist = {
			"projectred-core:resource_item",
			"microblockcbe:microblock",
			"microblockcbe:saw_stone",
			"microblockcbe:saw_iron",
			"microblockcbe:saw_diamond",
			"microblockcbe:stone_rod"
	};
	*/

    @SubscribeEvent
    public static void handleRecipes(RegistryEvent.Register<IRecipe> event) {
    	IForgeRegistryModifiable modRegistry = (IForgeRegistryModifiable) event.getRegistry();
    	String[] blacklist = recipeBlacklist;
    	for (int i = 0; i < blacklist.length; i++) {
            Main.logger.info("Scrubbing Recipe: " + blacklist[i]);
        	ResourceLocation target = new ResourceLocation(blacklist[i]);
            modRegistry.remove(target);
    	}
    }
    
    /*
    @SubscribeEvent
    public static void handleBlocks(RegistryEvent.Register<Block> event) {
    	IForgeRegistryModifiable modRegistry = (IForgeRegistryModifiable) event.getRegistry();
    	String[] blacklist = blockBlacklist;
    	for (int i = 0; i < blacklist.length; i++) {
            Main.logger.info("Scrubbing Block: " + blacklist[i]);
        	ResourceLocation target = new ResourceLocation(blacklist[i]);
        	Block scrub = Blocks.GLASS;
        	scrub.setRegistryName(blacklist[i]);
            modRegistry.register(scrub);
    	}
    }
    
    @SubscribeEvent
    public static void handleItems(RegistryEvent.Register<Item> event) {
    	IForgeRegistryModifiable modRegistry = (IForgeRegistryModifiable) event.getRegistry();
    	String[] blacklist = itemBlacklist;
    	for (int i = 0; i < blacklist.length; i++) {
            Main.logger.info("Scrubbing Item: " + blacklist[i]);
        	Item scrub = new Item();
        	scrub.setRegistryName(blacklist[i]);
        	ResourceLocation target = new ResourceLocation(blacklist[i]);
            modRegistry.register(scrub);
    	}
    }
    */
}
