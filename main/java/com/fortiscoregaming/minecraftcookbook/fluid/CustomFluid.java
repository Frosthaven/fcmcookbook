package com.fortiscoregaming.minecraftcookbook.fluid;

import com.fortiscoregaming.minecraftcookbook.Main;

import net.minecraft.block.material.Material;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.event.FMLInterModComms;

public class CustomFluid extends Fluid
{
    protected int mapColor = 0xFFFFFFFF;
    protected float overlayAlpha = 0.2F;
    protected static SoundEvent emptySound = SoundEvents.ITEM_BUCKET_EMPTY_LAVA;
    protected static SoundEvent fillSound = SoundEvents.ITEM_BUCKET_FILL_LAVA;
    protected Material material = Material.LAVA;
    protected boolean bucketEnabled = false;
 
    public CustomFluid(String fluidName, ResourceLocation still, ResourceLocation flowing) 
    {
        super(fluidName, still, flowing);
    }
 
    public CustomFluid(String fluidName, ResourceLocation still, ResourceLocation flowing, int mapColor) 
    {
        this(fluidName, still, flowing);
        setColor(mapColor);
    }
 
    public CustomFluid(String fluidName, ResourceLocation still, ResourceLocation flowing, int mapColor, float overlayAlpha)
    {
        this(fluidName, still, flowing, mapColor);
        setAlpha(overlayAlpha);
    }

	@Override
    public int getColor()
    {
        return mapColor;
    }

    public CustomFluid setColor(int parColor)
    {
        mapColor = parColor;
        return this;
    }
 
    public float getAlpha()
    {
        return overlayAlpha;
    }
 
    public CustomFluid setAlpha(float parOverlayAlpha)
    {
        overlayAlpha = parOverlayAlpha;
        return this;
    }
 
    @Override
    public CustomFluid setEmptySound(SoundEvent parSound)
    {
        emptySound = parSound;
        return this;
    }
 
    @Override
    public SoundEvent getEmptySound()
    {
        return emptySound;
    }
 
    @Override
    public CustomFluid setFillSound(SoundEvent parSound)
    {
        fillSound = parSound;
        return this;
    }
 
    @Override
    public SoundEvent getFillSound()
    {
        return fillSound;
    }
 
    public CustomFluid setMaterial(Material parMaterial)
    {
        material = parMaterial;
        return this;
    }
 
    public Material getMaterial()
    {
        return material;
    }
 
    @Override
    public boolean doesVaporize(FluidStack fluidStack)
    {
        if (block == null)
            return false;
        return block.getDefaultState().getMaterial() == getMaterial();
    }
    
    public CustomFluid setHasBucket(boolean parEnableBucket)
    {
        bucketEnabled = parEnableBucket;
        return this;
    }
    
    public boolean isBucketEnabled()
    {
        return bucketEnabled;
    }
    
    public CustomFluid register() {
        FluidRegistry.registerFluid(this);
        if (this.isBucketEnabled())
        {
            FluidRegistry.addBucketForFluid(this);
        }
		return this;
    }
}
