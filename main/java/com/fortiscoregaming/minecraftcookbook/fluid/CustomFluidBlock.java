package com.fortiscoregaming.minecraftcookbook.fluid;

import com.fortiscoregaming.minecraftcookbook.Main;

import net.minecraft.block.material.Material;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CustomFluidBlock extends BlockFluidClassic {

	public CustomFluidBlock(Fluid fluid, Material material) {
		super(fluid, material);
		setRegistryName(Main.MODID + ":" + fluid.getName());
		setUnlocalizedName(getRegistryName().toString());
		//GameRegistry.register(this);
		// @todo reference: https://jabelarminecraft.blogspot.com/p/minecraft-modding-custom-fluid-blocks.html
	}

	
}