package com.fortiscoregaming.minecraftcookbook.fluid;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.MaterialLiquid;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.fortiscoregaming.minecraftcookbook.Main;
import com.google.common.collect.ImmutableSet;

public class CustomFluids {
	public static Fluid preference(String fluidName, CustomFluid fluid) {
		if (FluidRegistry.isFluidRegistered(fluidName)) {
			Main.logger.info("Using external fluid: `" + fluidName + "`");
			return FluidRegistry.getFluid(fluidName);
		} else {
			Main.logger.info("Using built-in fluid: `" + fluid.getName() + "`");
			return fluid;
		}
	}

	public static final CustomFluid MOLTENDIAMOND = 
		(CustomFluid) new CustomFluid("diamond",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.CYAN))
		.setHasBucket(true)
		.setDensity(2000)
		.setGaseous(false)
		.setLuminosity(10)
		.setViscosity(10000)
		.setTemperature(1000)
		.setColor(0xFF7cebff);
	
	public static final CustomFluid MOLTENCERTUS = 
		(CustomFluid) new CustomFluid("certus",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.CYAN))
		.setHasBucket(true)
		.setDensity(2000)
		.setGaseous(false)
		.setLuminosity(7)
		.setViscosity(10000)
		.setTemperature(1000)
		.setColor(0xAA7cebff);
	
	public static final CustomFluid MOLTENCHARGEDCERTUS = 
		(CustomFluid) new CustomFluid("charged_certus",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.CYAN))
		.setHasBucket(true)
		.setDensity(2000)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(10000)
		.setTemperature(2000)
		.setColor(0xCC8efdff);
	
	public static final CustomFluid MOLTENSILICON =
		(CustomFluid) new CustomFluid("silicon",
		new ResourceLocation("tconstruct:blocks/fluids/milk"),
		new ResourceLocation("tconstruct:blocks/fluids/milk_flow"))
		.setMaterial(new MaterialLiquid(MapColor.GRAY))
		.setHasBucket(true)
		.setDensity(2000)
		.setGaseous(false)
		.setLuminosity(7)
		.setViscosity(10000)
		.setTemperature(1000)
		.setColor(0xD0333333);
	
	public static final CustomFluid MOLTENSILICONRED =
		(CustomFluid) new CustomFluid("silicon_red",
		new ResourceLocation("tconstruct:blocks/fluids/milk"),
		new ResourceLocation("tconstruct:blocks/fluids/milk_flow"))
		.setMaterial(new MaterialLiquid(MapColor.RED))
		.setHasBucket(true)
		.setDensity(2000)
		.setGaseous(false)
		.setLuminosity(7)
		.setViscosity(10000)
		.setTemperature(1000)
		.setColor(0xD0330000);
	
	public static final CustomFluid MOLTENSILICONYELLOW =
		(CustomFluid) new CustomFluid("silicon_yellow",
		new ResourceLocation("tconstruct:blocks/fluids/milk"),
		new ResourceLocation("tconstruct:blocks/fluids/milk_flow"))
		.setMaterial(new MaterialLiquid(MapColor.YELLOW))
		.setHasBucket(true)
		.setDensity(2000)
		.setGaseous(false)
		.setLuminosity(7)
		.setViscosity(10000)
		.setTemperature(1000)
		.setColor(0xD0333300);
	
	public static final CustomFluid MOLTENSILICONBLUE =
		(CustomFluid) new CustomFluid("silicon_blue",
		new ResourceLocation("tconstruct:blocks/fluids/milk"),
		new ResourceLocation("tconstruct:blocks/fluids/milk_flow"))
		.setMaterial(new MaterialLiquid(MapColor.BLUE))
		.setHasBucket(true)
		.setDensity(2000)
		.setGaseous(false)
		.setLuminosity(7)
		.setViscosity(10000)
		.setTemperature(1000)
		.setColor(0xD0000033);
	
	public static final CustomFluid RESIN = 
		(CustomFluid) new CustomFluid("resin",
		new ResourceLocation("tconstruct:blocks/fluids/milk"),
		new ResourceLocation("tconstruct:blocks/fluids/milk_flow"))
		.setMaterial(new MaterialLiquid(MapColor.ORANGE_STAINED_HARDENED_CLAY))
		.setHasBucket(true)
		.setDensity(2000)
		.setGaseous(false)
		.setLuminosity(7)
		.setViscosity(10000)
		.setTemperature(1000)
		.setColor(0xD0E7D02D);
	
	public static final CustomFluid MOLTENGLOWSTONE = 
		(CustomFluid) new CustomFluid("glowstone",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.ORANGE_STAINED_HARDENED_CLAY))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(350)
		.setColor(0xDDffcc00);
	
	public static final CustomFluid MOLTENCOAL = 
		(CustomFluid) new CustomFluid("coal",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.BLACK))
		.setHasBucket(true)
		.setDensity(900)
		.setGaseous(false)
		.setLuminosity(0)
		.setViscosity(2000)
		.setTemperature(2000)
		.setColor(0xFF222222);
	
	public static final CustomFluid MOLTENCINNABAR = 
		(CustomFluid) new CustomFluid("cinnabar",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.SILVER))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(350)
		.setColor(0xEECCCCCC);

	public static final CustomFluid MOLTENQUARTZ = 
		(CustomFluid) new CustomFluid("quartz",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.SILVER))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(350)
		.setColor(0xAAEDEBE5);

	public static final CustomFluid MOLTENFLUIX = 
		(CustomFluid) new CustomFluid("fluix",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.MAGENTA))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(3000)
		.setColor(0xDD6F2E6F);

	public static final CustomFluid MOLTENTHAUMIUM = 
		(CustomFluid) new CustomFluid("thaumium",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.MAGENTA))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(500)
		.setColor(0xFF5019B7);

	public static final CustomFluid MOLTENVOID = 
		(CustomFluid) new CustomFluid("void_metal",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.PURPLE))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(500)
		.setColor(0xFF120136);
	
	public static final CustomFluid MOLTENREDSTONE = 
		(CustomFluid) new CustomFluid("redstone",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.RED))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(7)
		.setViscosity(1500)
		.setTemperature(350)
		.setColor(0xFFFF0000);
	
	public static final CustomFluid MOLTENLAPIS = 
		(CustomFluid) new CustomFluid("lapis",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.BLUE))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(350)
		.setColor(0xFF0000FF);
	
	public static final CustomFluid MOLTENREDSTONEALLOY =
		(CustomFluid) new CustomFluid("redstone_alloy",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.RED))
		.setHasBucket(true)
		.setDensity(1000)
		.setGaseous(false)
		.setLuminosity(10)
		.setViscosity(7000)
		.setTemperature(700)
		.setColor(0xFFFF5555);

	public static final CustomFluid MOLTENELECTROTINE = 
		(CustomFluid) new CustomFluid("electrotine",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.BLUE))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(350)
		.setColor(0xFF36A0DA);

	public static final CustomFluid MOLTENELECTROTINEALLOY = 
		(CustomFluid) new CustomFluid("electrotine_alloy",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.LIGHT_BLUE))
		.setHasBucket(true)
		.setDensity(1000)
		.setGaseous(false)
		.setLuminosity(10)
		.setViscosity(7000)
		.setTemperature(700)
		.setColor(0xFF7cc3ea);
	
	public static final CustomFluid MOLTENURANIUM =
		(CustomFluid) new CustomFluid("uranium",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.GREEN))
		.setHasBucket(true)
		.setDensity(1000)
		.setGaseous(false)
		.setLuminosity(10)
		.setViscosity(25000)
		.setTemperature(1000)
		.setColor(0xFF54AC08);
	
	public static final CustomFluid IRRADIANTURANIUM = 
		(CustomFluid) new CustomFluid("irradiant_uranium",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.GREEN))
		.setHasBucket(true)
		.setDensity(2000)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(40000)
		.setTemperature(2000)
		.setColor(0xFFB2EE0E);
	
	public static final CustomFluid MOLTENMANASTEEL = 
		(CustomFluid) new CustomFluid("manasteel",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.BLUE))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(350)
		.setColor(0xEE0055CC);
	
	public static final CustomFluid MOLTENTERRASTEEL = 
		(CustomFluid) new CustomFluid("terrasteel",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.GREEN))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(350)
		.setColor(0xEE00CC00);
	
	public static final CustomFluid MOLTENELVENELEMENTIUM = 
		(CustomFluid) new CustomFluid("elven_elementium",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.PINK))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(350)
		.setColor(0xEECC00CC);
	
	public static final CustomFluid MOLTENGAIA = 
		(CustomFluid) new CustomFluid("gaia",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.GRAY))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(350)
		.setColor(0xEECCCCCC);
	
	public static final CustomFluid MOLTENDRAGONSTONE = 
		(CustomFluid) new CustomFluid("dragonstone",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.PINK))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(350)
		.setColor(0xEEff75ee);
	
	public static final CustomFluid MOLTENMANADIAMOND = 
		(CustomFluid) new CustomFluid("manadiamond",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.CYAN))
		.setHasBucket(true)
		.setDensity(1200)
		.setGaseous(false)
		.setLuminosity(15)
		.setViscosity(1500)
		.setTemperature(350)
		.setColor(0xEE4992ff);

	public static final CustomFluid MOLTENCORALIUM = 
		(CustomFluid) new CustomFluid("coralium",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.CYAN))
		.setHasBucket(true)
		.setDensity(2000)
		.setGaseous(false)
		.setLuminosity(10)
		.setViscosity(10000)
		.setTemperature(1000)
		.setColor(0xFF28A96B);

	public static final CustomFluid MOLTENDREADIUM = 
		(CustomFluid) new CustomFluid("dreadium",
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
		new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
		.setMaterial(new MaterialLiquid(MapColor.RED))
		.setHasBucket(true)
		.setDensity(2000)
		.setGaseous(false)
		.setLuminosity(10)
		.setViscosity(10000)
		.setTemperature(1000)
		.setColor(0xFFCA0501);

		public static final CustomFluid MOLTENABYSSALNITE = 
			(CustomFluid) new CustomFluid("abyssalnite",
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
			.setMaterial(new MaterialLiquid(MapColor.PURPLE))
			.setHasBucket(true)
			.setDensity(2000)
			.setGaseous(false)
			.setLuminosity(10)
			.setViscosity(10000)
			.setTemperature(1000)
			.setColor(0xFF602FA0);

		public static final CustomFluid MOLTENETHAXIUM = 
			(CustomFluid) new CustomFluid("ethaxium",
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
			.setMaterial(new MaterialLiquid(MapColor.PURPLE))
			.setHasBucket(true)
			.setDensity(2000)
			.setGaseous(false)
			.setLuminosity(10)
			.setViscosity(10000)
			.setTemperature(1000)
			.setColor(0xFFFFFFFF);
		
		public static final CustomFluid MOLTENSALT =
			(CustomFluid) new CustomFluid("salt",
			new ResourceLocation("tconstruct:blocks/fluids/milk"),
			new ResourceLocation("tconstruct:blocks/fluids/milk_flow"))
			.setMaterial(new MaterialLiquid(MapColor.RED))
			.setHasBucket(true)
			.setDensity(2000)
			.setGaseous(false)
			.setLuminosity(7)
			.setViscosity(10000)
			.setTemperature(350)
			.setColor(0xDDDCD4D4);
		
		public static final CustomFluid MOLTENCYANITE =
			(CustomFluid) new CustomFluid("cyanite",
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
			.setMaterial(new MaterialLiquid(MapColor.CYAN))
			.setHasBucket(true)
			.setDensity(2000)
			.setGaseous(false)
			.setLuminosity(7)
			.setViscosity(10000)
			.setTemperature(350)
			.setColor(0xFF005EA7);
		
		public static final CustomFluid LIQUIDGRAPHITE = 
			(CustomFluid) new CustomFluid("graphite",
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
			.setMaterial(new MaterialLiquid(MapColor.GRAY))
			.setHasBucket(true)
			.setDensity(900)
			.setGaseous(false)
			.setLuminosity(0)
			.setViscosity(2000)
			.setTemperature(350)
			.setColor(0xFF2C2C2C);
		
		public static final CustomFluid MOLTENBLUETONIUM =
			(CustomFluid) new CustomFluid("blutonium",
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
			.setMaterial(new MaterialLiquid(MapColor.BLUE))
			.setHasBucket(true)
			.setDensity(2000)
			.setGaseous(false)
			.setLuminosity(7)
			.setViscosity(10000)
			.setTemperature(1500)
			.setColor(0xFF1300A1);
		
		public static final CustomFluid MOLTENLUDICRITE =
			(CustomFluid) new CustomFluid("ludicrite",
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
			.setMaterial(new MaterialLiquid(MapColor.MAGENTA))
			.setHasBucket(true)
			.setDensity(2000)
			.setGaseous(false)
			.setLuminosity(7)
			.setViscosity(10000)
			.setTemperature(2500)
			.setColor(0xFFCD1ABE);
		
		public static final CustomFluid MOLTENDRACONIUM =
			(CustomFluid) new CustomFluid("draconium",
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
			.setMaterial(new MaterialLiquid(MapColor.PURPLE))
			.setHasBucket(true)
			.setDensity(2000)
			.setGaseous(false)
			.setLuminosity(7)
			.setViscosity(10000)
			.setTemperature(2500)
			.setColor(0xFF512874);
			
			public static final CustomFluid MOLTENDRACONIUMAWAKENED =
				(CustomFluid) new CustomFluid("draconiumawakened",
				new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
				new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
				.setMaterial(new MaterialLiquid(MapColor.ORANGE_STAINED_HARDENED_CLAY))
				.setHasBucket(true)
				.setDensity(2000)
				.setGaseous(false)
				.setLuminosity(7)
				.setViscosity(10000)
				.setTemperature(2500)
				.setColor(0xFFD63C00);
			
			public static final CustomFluid MOLTENMITHRIL =
				(CustomFluid) new CustomFluid("mithril",
				new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
				new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
				.setMaterial(new MaterialLiquid(MapColor.ORANGE_STAINED_HARDENED_CLAY))
				.setHasBucket(true)
				.setDensity(2000)
				.setGaseous(false)
				.setLuminosity(7)
				.setViscosity(10000)
				.setTemperature(900)
				.setColor(0xFF6E6BFF);
		
		public static final CustomFluid MOLTENASTRALSTARMETAL =
			(CustomFluid) new CustomFluid("astralstarmetal",
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
			.setMaterial(new MaterialLiquid(MapColor.BLUE))
			.setHasBucket(true)
			.setDensity(2000)
			.setGaseous(false)
			.setLuminosity(9)
			.setViscosity(10000)
			.setTemperature(2500)
			.setColor(0xFF01144D);
		
		public static final CustomFluid MELTEDCHEESE =
			(CustomFluid) new CustomFluid("cheese",
			new ResourceLocation("tconstruct:blocks/fluids/milk"),
			new ResourceLocation("tconstruct:blocks/fluids/milk_flow"))
			.setMaterial(new MaterialLiquid(MapColor.YELLOW))
			.setHasBucket(true)
			.setDensity(2000)
			.setGaseous(false)
			.setLuminosity(0)
			.setViscosity(10000)
			.setTemperature(50)
			.setColor(0xDDFFF536);
		
		public static final CustomFluid DAIRY =
			(CustomFluid) new CustomFluid("dairy",
			new ResourceLocation("tconstruct:blocks/fluids/milk"),
			new ResourceLocation("tconstruct:blocks/fluids/milk_flow"))
			.setMaterial(new MaterialLiquid(MapColor.GOLD))
			.setHasBucket(true)
			.setDensity(2000)
			.setGaseous(false)
			.setLuminosity(0)
			.setViscosity(10000)
			.setTemperature(50)
			.setColor(0xFFFFD86B);


		public static final CustomFluid MOLTENDESH = 
			(CustomFluid) new CustomFluid("desh",
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
			.setMaterial(new MaterialLiquid(MapColor.MAGENTA))
			.setHasBucket(true)
			.setDensity(1200)
			.setGaseous(false)
			.setLuminosity(8)
			.setViscosity(1500)
			.setTemperature(500)
			.setColor(0xFF6A6A6A);


		public static final CustomFluid MOLTENTITANIUM = 
			(CustomFluid) new CustomFluid("titanium",
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
			new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
			.setMaterial(new MaterialLiquid(MapColor.BLACK))
			.setHasBucket(true)
			.setDensity(1200)
			.setGaseous(false)
			.setLuminosity(8)
			.setViscosity(1500)
			.setTemperature(500)
			.setColor(0xFF2D3648);

			public static final CustomFluid MOLTENDARK = 
				(CustomFluid) new CustomFluid("dark",
				new ResourceLocation("tconstruct:blocks/fluids/molten_metal"),
				new ResourceLocation("tconstruct:blocks/fluids/molten_metal_flow"))
				.setMaterial(new MaterialLiquid(MapColor.BLACK))
				.setHasBucket(true)
				.setDensity(1200)
				.setGaseous(false)
				.setLuminosity(8)
				.setViscosity(1500)
				.setTemperature(500)
				.setColor(0xFF353535);

    public static final ImmutableSet<CustomFluid> SET_FLUIDS = ImmutableSet.of(
		MOLTENDIAMOND,
		MOLTENCERTUS,
		MOLTENCHARGEDCERTUS,
		MOLTENSILICON,
		MOLTENSILICONRED,
		MOLTENSILICONYELLOW,
		MOLTENSILICONBLUE,
		RESIN,
		MOLTENGLOWSTONE,
		MOLTENQUARTZ,
		MOLTENFLUIX,
		MOLTENCINNABAR,
        MOLTENREDSTONE,
        MOLTENREDSTONEALLOY,
        MOLTENLAPIS,
        MOLTENELECTROTINE,
        MOLTENELECTROTINEALLOY,
        MOLTENURANIUM,
        IRRADIANTURANIUM,
        MOLTENTHAUMIUM,
        MOLTENVOID,
        MOLTENCOAL,
        MOLTENMANASTEEL,
        MOLTENTERRASTEEL,
        MOLTENELVENELEMENTIUM,
        MOLTENGAIA,
        MOLTENDRAGONSTONE,
        MOLTENMANADIAMOND,
        MOLTENCORALIUM,
        MOLTENDREADIUM,
        MOLTENABYSSALNITE,
        MOLTENETHAXIUM,
        MOLTENSALT,
        MOLTENCYANITE,
        LIQUIDGRAPHITE,
        MOLTENBLUETONIUM,
        MOLTENLUDICRITE,
        MOLTENDRACONIUM,
        MOLTENDRACONIUMAWAKENED,
        MOLTENMITHRIL,
        MOLTENASTRALSTARMETAL,
        MELTEDCHEESE,
        DAIRY,
        MOLTENDESH,
        MOLTENTITANIUM,
        MOLTENDARK
    );

    public static void registerFluids() {
    	String logList = "";
        for (final CustomFluid fluid : SET_FLUIDS)
        {
        	if (!FluidRegistry.isFluidRegistered(fluid.getName())) {
	            FluidRegistry.registerFluid(fluid);
	            
	            if (fluid.isBucketEnabled()) {
		            FluidRegistry.addBucketForFluid(fluid);
	            }
            
	            logList = logList + "`" + fluid.getName() + "` ";
        	}
        }
        Main.logger.info("Registered Fluids: " + logList);
    }
}
