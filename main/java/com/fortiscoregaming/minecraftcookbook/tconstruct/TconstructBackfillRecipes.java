package com.fortiscoregaming.minecraftcookbook.tconstruct;

import com.fortiscoregaming.minecraftcookbook.tconstruct.lib.OreDictSmelterIntegration;

import slimeknights.tconstruct.shared.TinkerFluids;

public class TconstructBackfillRecipes {

	public static void registerRecipes() {

		// back-fill extras
		new OreDictSmelterIntegration("Alubrass", TinkerFluids.alubrass).setBackfillOnly().register();
		new OreDictSmelterIntegration("Aluminum", TinkerFluids.aluminum).setBackfillOnly().register();
		new OreDictSmelterIntegration("Ardite", TinkerFluids.ardite).setBackfillOnly().register();
		new OreDictSmelterIntegration("Brass", TinkerFluids.brass).setBackfillOnly().register();
		new OreDictSmelterIntegration("Bronze", TinkerFluids.bronze).setBackfillOnly().register();
		new OreDictSmelterIntegration("Clay", TinkerFluids.clay).setBackfillOnly().register();
		new OreDictSmelterIntegration("Cobalt", TinkerFluids.cobalt).setBackfillOnly().register();
		new OreDictSmelterIntegration("Copper", TinkerFluids.copper).setBackfillOnly().register();
		new OreDictSmelterIntegration("Dirt", TinkerFluids.dirt).setBackfillOnly().register();
		new OreDictSmelterIntegration("Electrum", TinkerFluids.electrum).setBackfillOnly().register();
		new OreDictSmelterIntegration("Emerald", TinkerFluids.emerald).setBackfillOnly().register();
		new OreDictSmelterIntegration("Glass", TinkerFluids.glass).setBackfillOnly().register();
		new OreDictSmelterIntegration("Gold", TinkerFluids.gold).setBackfillOnly().register();
		new OreDictSmelterIntegration("Iron", TinkerFluids.iron).setBackfillOnly().register();
		new OreDictSmelterIntegration("Knightslime", TinkerFluids.knightslime).setBackfillOnly().register();
		new OreDictSmelterIntegration("Lead", TinkerFluids.lead).setBackfillOnly().register();
		new OreDictSmelterIntegration("Manyullyn", TinkerFluids.manyullyn).setBackfillOnly().register();
		new OreDictSmelterIntegration("Nickel", TinkerFluids.nickel).setBackfillOnly().register();
		new OreDictSmelterIntegration("Obsidian", TinkerFluids.obsidian).setBackfillOnly().register();
		new OreDictSmelterIntegration("PigIron", TinkerFluids.pigIron).setBackfillOnly().register();
		new OreDictSmelterIntegration("SearedStone", TinkerFluids.searedStone).setBackfillOnly().register();
		new OreDictSmelterIntegration("Silver", TinkerFluids.silver).setBackfillOnly().register();
		new OreDictSmelterIntegration("Steel", TinkerFluids.steel).setBackfillOnly().register();
		new OreDictSmelterIntegration("Tin", TinkerFluids.tin).setBackfillOnly().register();
		new OreDictSmelterIntegration("Zinc", TinkerFluids.zinc).setBackfillOnly().register();
		new OreDictSmelterIntegration("Blood", TinkerFluids.blood).setBackfillOnly().register();
		new OreDictSmelterIntegration("Blueslime", TinkerFluids.blueslime).setBackfillOnly().register();
		new OreDictSmelterIntegration("Milk", TinkerFluids.milk).setBackfillOnly().register();

		new OreDictSmelterIntegration("Platinum", TconstructOreDictRecipes.fluidByName("platinum")).setBackfillOnly().register();
		new OreDictSmelterIntegration("Iridium", TconstructOreDictRecipes.fluidByName("iridium")).setBackfillOnly().register();
		new OreDictSmelterIntegration("Mana", TconstructOreDictRecipes.fluidByName("mana")).setBackfillOnly().register();
		new OreDictSmelterIntegration("Invar", TconstructOreDictRecipes.fluidByName("invar")).setBackfillOnly().register();
		new OreDictSmelterIntegration("Constantan", TconstructOreDictRecipes.fluidByName("constantan")).setBackfillOnly().register();
		new OreDictSmelterIntegration("Signalum", TconstructOreDictRecipes.fluidByName("signalum")).setBackfillOnly().register();
		new OreDictSmelterIntegration("Lumium", TconstructOreDictRecipes.fluidByName("platinum")).setBackfillOnly().register();
		new OreDictSmelterIntegration("Enderium", TconstructOreDictRecipes.fluidByName("platinum")).setBackfillOnly().register();
		
	}

}
