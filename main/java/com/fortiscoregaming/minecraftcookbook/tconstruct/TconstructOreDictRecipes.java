package com.fortiscoregaming.minecraftcookbook.tconstruct;

import com.fortiscoregaming.minecraftcookbook.Main;
import com.fortiscoregaming.minecraftcookbook.fluid.CustomFluid;
import com.fortiscoregaming.minecraftcookbook.fluid.CustomFluids;
import com.fortiscoregaming.minecraftcookbook.tconstruct.lib.OreDictSmelterIntegration;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.oredict.OreDictionary;
import slimeknights.tconstruct.library.TinkerRegistry;
import slimeknights.tconstruct.library.materials.Material;
import slimeknights.tconstruct.shared.TinkerFluids;

public class TconstructOreDictRecipes {
	public static void saveToRegistry(String name, String resourceName, int meta) {
		Item item = Item.REGISTRY.getObject(new ResourceLocation(resourceName));
		OreDictionary.registerOre(name, new ItemStack(item, 1, meta));
	}
	public static void saveToRegistry(String name, Item item) {
		ItemStack stack = new ItemStack(item);
		OreDictionary.registerOre(name, stack);
	}
	public static void saveToRegistry(String name, Block block) {
		ItemStack stack = new ItemStack(block);
		OreDictionary.registerOre(name, stack);
	}
	public static void saveToRegistry(String name, ItemStack stack) {
		OreDictionary.registerOre(name, stack);
	}
	public static Fluid fluidByName(String fluidName) {
		return FluidRegistry.getFluid(fluidName);
	}

	public static void registerRecipes() {
		// BACKFILL
		TinkerRegistry.registerMelting(
				new ItemStack(Blocks.HARDENED_CLAY),
				fluidByName("clay"),
				576
			);
		TconstructBackfillRecipes.registerRecipes();

		// LAVA
		saveToRegistry("blockLava", Blocks.MAGMA);
		saveToRegistry("rodLava", Items.BLAZE_ROD);
		saveToRegistry("dustLava", Items.BLAZE_POWDER);
		new OreDictSmelterIntegration("Lava", fluidByName("lava"))
		.setValDust(72)
		.register();

		// DIAMOND
    	new OreDictSmelterIntegration("Diamond", fluidByName("diamond"))
    	.register();
    	
    	// REDSTONE
    	new OreDictSmelterIntegration("Redstone", fluidByName("redstone"))
    	.setValBlock(900)
    	.setValDust(100)
    	.setCastless("dustRedstone").setValCastless(100)
		.melt("oreClathrateRedstone", 1000)
    	.setFuel(38, 150)
    	.register();
    	
    	// REDSTONE ALLOY
    	new OreDictSmelterIntegration("RedAlloy", fluidByName("redstone_alloy"))
    	.register();
    	
    	// LAPIS LAZULI
    	new OreDictSmelterIntegration("Lapis", fluidByName("lapis"))
    	.register();
    	
    	// GLOWSTONE
		new OreDictSmelterIntegration("Glowstone", fluidByName("glowstone"))
		.setCastless("dustGlowstone")
		.setBlock("glowstone").setValBlock(1000)
		.setValCastless(250)
		.setValDust(250)
		.melt("oreClathrateGlowstone", 1000)
    	.register();
    	
		// ELECTROTINE
    	new OreDictSmelterIntegration("Electrotine", fluidByName("electrotine"))
    	.setFuel(38, 150)
    	.register();
    	
    	new OreDictSmelterIntegration("ElectrotineAlloy", fluidByName("electrotine_alloy"))
    	.register();
    	
    	// COAL
        saveToRegistry("coal", "minecraft:coal", 0);
    	new OreDictSmelterIntegration("Coal", fluidByName("coal"))
    	.setValBlock(900)
    	.setValDust(100)
    	.setCastless("coal").setValCastless(100)
    	.register();
    	
    	// CINNABAR
    	OreDictSmelterIntegration cinnabar = new OreDictSmelterIntegration("Cinnabar", fluidByName("cinnabar"));
    	if (Loader.isModLoaded("thaumcraft")) {
    		cinnabar.setNugget("nuggetQuicksilver").setCastless("quicksilver");
    	}
    	cinnabar.register();
    	
    	// FLUIX
    	OreDictSmelterIntegration fluix = new OreDictSmelterIntegration("Fluix", fluidByName("fluix"));
    	if (Loader.isModLoaded("appliedenergistics2")) {
            saveToRegistry("gemFluix", "appliedenergistics2:material", 7);
            saveToRegistry("blockFluix", "appliedenergistics2:fluix_block", 0);
            saveToRegistry("pureCrystalFluix", "appliedenergistics2:material", 12);
            fluix.melt("pureCrystalFluix", 162);
    	}
    	fluix.setValGem(324).setValDust(324).setFuel(8,300).register();

    	// CERTUS QUARTZ
    	OreDictSmelterIntegration certus = new OreDictSmelterIntegration("CertusQuartz", fluidByName("certus"));
    	OreDictSmelterIntegration chargedcertus = new OreDictSmelterIntegration("ChargedCertusQuartz", fluidByName("charged_certus"));
    	if (Loader.isModLoaded("appliedenergistics2")) {
            saveToRegistry("gemCertusQuartz", "appliedenergistics2:material", 0);
            saveToRegistry("gemChargedCertusQuartz", "appliedenergistics2:material", 1);
            saveToRegistry("overrideFixoreCertusQuartz", "appliedenergistics2:quartz_ore", 0);
            saveToRegistry("overrideFixoreChargedCertusQuartz", "appliedenergistics2:charged_quartz_ore", 0);
            saveToRegistry("pureCrystalCertusQuartz", "appliedenergistics2:material", 10);
            certus.melt("pureCrystalCertusQuartz", 162);
    	}
    	certus.setOre("overrideFixoreCertusQuartz").register();
    	chargedcertus.setOre("overrideFixoreChargedCertusQuartz").setFuel(10, 250).register();

    	// QUARTZ
    	OreDictSmelterIntegration quartz = new OreDictSmelterIntegration("Quartz", fluidByName("quartz"));
    	if (Loader.isModLoaded("appliedenergistics2")) {
    		saveToRegistry("dustQuartz", "appliedenergistics2:material", 3);
            saveToRegistry("pureCrystalQuartz", "appliedenergistics2:material", 11);
            quartz.melt("pureCrystalQuartz", 162);
    	}
    	quartz.register();
    	
    	// SILICON
    	if (Loader.isModLoaded("projectred-core")) {
            saveToRegistry("itemSilicon", "projectred-core:resource_item", 301);
            saveToRegistry("bouleSilicon", "projectred-core:resource_item", 300);
    	}
    	new OreDictSmelterIntegration("Silicon", fluidByName("silicon"))
		.setCastless("itemSilicon").register();
    	
    	// RED SILICON
    	if (Loader.isModLoaded("projectred-core")) {
    		saveToRegistry("ballRedSilicon", "projectred-core:resource_item", 320);
    	}
    	new OreDictSmelterIntegration("RedSilicon", fluidByName("silicon_red"))
		.setCastless("ballRedSilicon").register();
    	
    	// YELLOW SILICON
    	if (Loader.isModLoaded("projectred-core")) {
    		saveToRegistry("ballYellowSilicon", "projectred-core:resource_item", 341);
    	}
    	new OreDictSmelterIntegration("YellowSilicon", fluidByName("silicon_yellow"))
		.setCastless("ballYellowSilicon").register();
    	
    	// BLUE SILICON
    	if (Loader.isModLoaded("projectred-core")) {
    		saveToRegistry("ballBlueSilicon", "projectred-core:resource_item", 342);
    	}
    	new OreDictSmelterIntegration("BlueSilicon", fluidByName("silicon_blue"))
		.setCastless("ballBlueSilicon").register();
    	
    	// RESIN
    	if (Loader.isModLoaded("ic2")) {
    		saveToRegistry("ballResin", "ic2:misc_resource", 4);
    		saveToRegistry("ballRubber", "ic2:crafting", 0);
    	}
    	new OreDictSmelterIntegration("Resin", fluidByName("resin"))
    	.setDust("ballResin").setCastless("ballRubber").setValCastless(72).register();

    	// THAUMIUM
    	new OreDictSmelterIntegration("Thaumium", fluidByName("thaumium"))
    	.register();
    	
    	// VOID METAL
    	OreDictSmelterIntegration metalvoid = new OreDictSmelterIntegration("Void", fluidByName("void_metal"));
    	if (Loader.isModLoaded("thaumcraft")) {
    		saveToRegistry("blockVoid", "thaumcraft:metal_void", 0);
    	}
    	metalvoid.register();
    	
    	// URANIUM
    	new OreDictSmelterIntegration("Uranium", fluidByName("uranium"))
    	.setFuel(25, 200)
    	.register();

    	// IRRADIANT URANIUM
    	OreDictSmelterIntegration irradianturanium = new OreDictSmelterIntegration("IrradiantUranium", fluidByName("irradiant_uranium"));
		if (Loader.isModLoaded("advanced_solar_panels")) {
			saveToRegistry("ingotIrradiantUranium", "advanced_solar_panels:crafting", 3);
    	}
		irradianturanium.setFuel(10, 250).register();
		
		// UUMATTER
		if (Loader.isModLoaded("ic2")) {
			OreDictSmelterIntegration uuMatter = new OreDictSmelterIntegration("Uumatter", fluidByName("ic2uu_matter"));
            saveToRegistry("ballUumatter", "ic2:misc_resource", 3);            
            uuMatter.setCastless("ballUumatter").register();
		}
		
		// AEROTHEUM
		if (FluidRegistry.isFluidRegistered("aerotheum")) {
			new OreDictSmelterIntegration("Aerotheum", fluidByName("aerotheum"))
			.setValDust(250)
			.register();
		}
		
		// BIOCRUDE
		if (FluidRegistry.isFluidRegistered("biocrude")) {
			new OreDictSmelterIntegration("Biocrude", fluidByName("biocrude"))
			.melt("itemBiomass", 100)
			.melt("itemBiomassRich", 150)
			.melt("itemBioblend", 100)
			.melt("itemBioblendRich", 150)
			.register();
		}
		
		// CRUDE OIL
		if (Loader.isModLoaded("thermalfoundation")) {
			
			String oilname = "crude_oil";

			if (Loader.isModLoaded("immersiveengineering")) {
				oilname = "oil";
			}

			saveToRegistry("bitumen","thermalfoundation:material", 892);
			new OreDictSmelterIntegration("Oil", fluidByName(oilname))
			.melt("oreClathrateOilShale", 1000)
			.melt("oreClathrateOilSand", 1000)
			.melt("clathrateOil", 144)
			.melt("bitumen", 144)
			.register();
		}
		
		// CRYOTHEUM
		if (FluidRegistry.isFluidRegistered("cryotheum")) {
			new OreDictSmelterIntegration("Cryotheum", fluidByName("cryotheum"))
			.setValDust(250)
			.register();
		}
		
		// ENDER (Resonant Ender)
		if (FluidRegistry.isFluidRegistered("ender")) {
			new OreDictSmelterIntegration("Ender", fluidByName("ender"))
			.melt("enderpearl", 250)
			.melt("oreClathrateEnder", 1000)
			.register();
		}
		
		// PETROTHEUM
		if (FluidRegistry.isFluidRegistered("petrotheum")) {
			new OreDictSmelterIntegration("Petrotheum", fluidByName("petrotheum"))
			.setValDust(250)
			.register();
		}
		
		// PYROTHEUM
		if (FluidRegistry.isFluidRegistered("pyrotheum")) {
			new OreDictSmelterIntegration("Pyrotheum", fluidByName("pyrotheum"))
			.setValDust(250)
			.register();
		}
		
		// SALT PETER
		if (FluidRegistry.isFluidRegistered("salt")) {
			new OreDictSmelterIntegration("Saltpeter", fluidByName("salt"))
			.setValDust(250).setValCastless(250)
			.setCastless("dustSaltpeter")
			.register();
		}
		
		if (Loader.isModLoaded("abyssalcraft")) {
			//Liquified Coralium
			if (FluidRegistry.isFluidRegistered("coralium")) {
				new OreDictSmelterIntegration("LiquifiedCoralium", fluidByName("coralium"))
				.register();
			}
			
			//Dreadium
			if (FluidRegistry.isFluidRegistered("dreadium")) {
				new OreDictSmelterIntegration("Dreadium", fluidByName("dreadium"))
				.register();
			}
			
			//Abyssalnite
			if (FluidRegistry.isFluidRegistered("abyssalnite")) {
				new OreDictSmelterIntegration("Abyssalnite", fluidByName("abyssalnite"))
				.melt("oreDreadedAbyssalnite", Material.VALUE_Ore())
				.register();
			}
			
			//Ethaxium
			if (FluidRegistry.isFluidRegistered("ethaxium")) {
				new OreDictSmelterIntegration("Ethaxium", fluidByName("ethaxium"))
				.register();
			}
		}
		
		if (Loader.isModLoaded("botania")) {
			
			//Manasteel
			if (FluidRegistry.isFluidRegistered("manasteel")) {
				saveToRegistry("blockManasteel","botania:storage",0);
				new OreDictSmelterIntegration("Manasteel", fluidByName("manasteel"))
				.register();
			}
			
			//Terrasteel
			if (FluidRegistry.isFluidRegistered("terrasteel")) {
				saveToRegistry("blockTerrasteel","botania:storage",1);
				new OreDictSmelterIntegration("Terrasteel", fluidByName("terrasteel"))
				.register();
			}
			
			//Elementium
			if (FluidRegistry.isFluidRegistered("elven_elementium")) {
				saveToRegistry("blockElvenElementium","botania:storage",2);
				new OreDictSmelterIntegration("ElvenElementium", fluidByName("elven_elementium"))
				.register();
			}
			
			//Gaia
			if (FluidRegistry.isFluidRegistered("gaia")) {
				new OreDictSmelterIntegration("Gaia", fluidByName("gaia"))
				.setIngot("gaiaIngot")
				.register();
			}
			
			//Mana Diamond
			if (FluidRegistry.isFluidRegistered("manadiamond")) {
				saveToRegistry("blockManaDiamond", "botania:storage",3);
				new OreDictSmelterIntegration("ManaDiamond", fluidByName("manadiamond"))
				.setGem("manaDiamond")
				.setBlock("blockManaDiamond")
				.register();
			}
			
			//Dragonstone
			if (FluidRegistry.isFluidRegistered("dragonstone")) {
				saveToRegistry("blockElvenDragonstone", "botania:storage",4);
				new OreDictSmelterIntegration("Dragonstone", fluidByName("dragonstone"))
				.setGem("elvenDragonstone")
				.setBlock("blockElvenDragonstone")
				.register();
			}
		}

		new OreDictSmelterIntegration("Cyanite", fluidByName("cyanite")).register();
		new OreDictSmelterIntegration("Graphite", fluidByName("graphite")).register();
		new OreDictSmelterIntegration("Blutonium", fluidByName("blutonium")).register();
		new OreDictSmelterIntegration("Ludicrite", fluidByName("ludicrite")).register();

		OreDictSmelterIntegration draconium = new OreDictSmelterIntegration("Draconium", fluidByName("draconium"));
		if (Loader.isModLoaded("draconicevolution")) {
			saveToRegistry("oreDraconiumAlt", "draconicevolution:draconium_ore", 0);
			saveToRegistry("oreDraconiumAlt", "draconicevolution:draconium_ore", 1);
			saveToRegistry("oreDraconiumAlt", "draconicevolution:draconium_ore", 2);
			
			draconium.melt("oreDraconiumAlt", Material.VALUE_Ore());
		}
		draconium.register();
	
		new OreDictSmelterIntegration("DraconiumAwakened", fluidByName("draconiumawakened")).register();
		new OreDictSmelterIntegration("Mithril", fluidByName("mithril")).register();
		new OreDictSmelterIntegration("AstralStarmetal", fluidByName("astralstarmetal")).register();

		if (Loader.isModLoaded("galacticraftplanets")) {

			new OreDictSmelterIntegration("Titanium", fluidByName("titanium"))
			.melt("oreIlmenite", Material.VALUE_Ore())
			.register();

			saveToRegistry("itemCheese","galacticraftcore:cheese_curd",0);
			new OreDictSmelterIntegration("Cheese", fluidByName("cheese"))
			.setCastless("itemCheese")
			.melt("itemCheese", 144)
			.register();

			saveToRegistry("itemDairy","galacticraftcore:cheese",0);
			new OreDictSmelterIntegration("Dairy", fluidByName("dairy"))
				.setCastless("itemDairy").setValCastless(1152)
				.register();			

			new OreDictSmelterIntegration("Desh", fluidByName("desh")).register();
		}
		
		if (Loader.isModLoaded("evilcraft")) {
			saveToRegistry("blockDark","evilcraft:dark_block",0);
			new OreDictSmelterIntegration("Dark", fluidByName("dark")).register();
			
			saveToRegistry("blockHardenedBlood","evilcraft:hardened_blood",0);
			saveToRegistry("shardHardenedBlood","evilcraft:hardened_blood_shard",0);
			new OreDictSmelterIntegration("HardenedBlood", fluidByName("evilcraftblood"))
			.setCastless("shardHardenedBlood").setValCastless(144)
			.register();
		}
		
	}

}
