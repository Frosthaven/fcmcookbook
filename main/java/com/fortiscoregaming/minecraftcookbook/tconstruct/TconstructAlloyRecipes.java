package com.fortiscoregaming.minecraftcookbook.tconstruct;

import com.fortiscoregaming.minecraftcookbook.fluid.CustomFluids;

import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import slimeknights.tconstruct.library.TinkerRegistry;
import slimeknights.tconstruct.shared.TinkerFluids;

public class TconstructAlloyRecipes {
	public static FluidStack fsByName(String fluidName, Integer amount) {
		return new FluidStack(FluidRegistry.getFluid(fluidName), amount);
	}

	public static void registerRecipes() {
		
		// REDSTONE ALLOY
		TinkerRegistry.registerAlloy(fsByName("redstone_alloy", 1),
			fsByName("redstone", 8),
			fsByName("iron", 1)
		);
		
		// ELECTROTINE ALLOY
		TinkerRegistry.registerAlloy(fsByName("electrotine_alloy", 1),
			fsByName("electrotine", 8),
			fsByName("iron", 1)
		);
		
		// IRRADIANT URANIUM
		TinkerRegistry.registerAlloy(fsByName("irradiant_uranium", 1),
			fsByName("uranium", 1),
			fsByName("glowstone", 9)
		);
		
		// SILICON
		TinkerRegistry.registerAlloy(fsByName("silicon", 2),
			fsByName("glass", 1),
			fsByName("quartz", 1)
		);
		TinkerRegistry.registerAlloy(fsByName("silicon", 2),
			fsByName("glass", 1),
			fsByName("certus", 1)
		);
		TinkerRegistry.registerAlloy(fsByName("silicon_red", 1),
			fsByName("silicon", 1),
			fsByName("redstone", 8)
		);
		TinkerRegistry.registerAlloy(fsByName("silicon_yellow", 1),
			fsByName("silicon", 1),
			fsByName("glowstone", 8)
		);
		TinkerRegistry.registerAlloy(fsByName("silicon_blue", 1),
			fsByName("silicon", 1),
			fsByName("electrotine", 8)
		);
		
		// RESIN
		TinkerRegistry.registerAlloy(fsByName("resin", 2),
			fsByName("silicon", 1),
			fsByName("lava", 1)
		);
		
		// FLUIX
		TinkerRegistry.registerAlloy(fsByName("fluix", 5),
			fsByName("quartz", 1),
			fsByName("certus", 1),
			fsByName("redstone", 1)
		);

		// GRAPHITE
		TinkerRegistry.registerAlloy(fsByName("graphite", 8),
			fsByName("clay", 1),
			fsByName("coal", 8)
		);
		
		// DAIRY
		TinkerRegistry.registerAlloy(fsByName("dairy", 8),
			fsByName("milk", 1),
			fsByName("cheese", 8)
		);
		
	}

}
