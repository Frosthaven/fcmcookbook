package com.fortiscoregaming.minecraftcookbook.tconstruct;

import java.lang.reflect.Field;

import com.fortiscoregaming.minecraftcookbook.Main;
import com.fortiscoregaming.minecraftcookbook.fluid.CustomFluids;
import com.fortiscoregaming.minecraftcookbook.tconstruct.lib.OreDictSmelterIntegration;

import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.oredict.OreDictionary;
import slimeknights.tconstruct.library.TinkerRegistry;
import slimeknights.tconstruct.shared.TinkerFluids;

public class InitSmeltery {
	public static void init() {
		TconstructOreDictRecipes.registerRecipes();
		TconstructAlloyRecipes.registerRecipes();
	}
}
