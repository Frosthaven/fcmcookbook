package com.fortiscoregaming.minecraftcookbook.tconstruct.lib;

import java.util.ArrayList;

import com.fortiscoregaming.minecraftcookbook.Main;
import com.fortiscoregaming.minecraftcookbook.fluid.CustomFluid;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.oredict.OreDictionary;
import slimeknights.mantle.util.RecipeMatch;
import slimeknights.tconstruct.library.TinkerRegistry;
import slimeknights.tconstruct.library.fluid.FluidColored;
import slimeknights.tconstruct.library.fluid.FluidMolten;
import slimeknights.tconstruct.library.materials.Material;
import slimeknights.tconstruct.library.smeltery.ICastingRecipe;
import slimeknights.tconstruct.library.smeltery.MeltingRecipe;
import blusunrize.immersiveengineering.api.ApiUtils;
import blusunrize.immersiveengineering.api.ComparableItemStack;
import blusunrize.immersiveengineering.api.IEApi;
import blusunrize.immersiveengineering.api.crafting.CrusherRecipe;
import blusunrize.immersiveengineering.api.crafting.MetalPressRecipe;
import blusunrize.immersiveengineering.common.*;
import blusunrize.immersiveengineering.common.util.Utils;
import cofh.thermalexpansion.util.managers.machine.CrucibleManager;

public class OreDictSmelterIntegration {
	private String oreDictName;
	private Fluid fluid = null;
	
	private boolean backfillOnly = false;
	private int valCastless = 144;
	private int valCoin = 48;
	private int valTiny = 16;
	private int valDust = 144;
	private int valGem = 144; // TC defaults this to 666, but usually gems are 1/9th a block instead
	private int valNugget = 16;
	private int valCrystal = 144;
	private int valCrystalShard = 16;
	private int valCluster = 288;
	private int valIngot = 144;
	private int valRod   = 144;
	private int valPlate = 144;
	private int valDensePlate = 1296;
	private int valBlock = 1296;
	private int valOre = Material.VALUE_Ore();
	private int valCrushed = Material.VALUE_Ore();
	private int valPurified = Material.VALUE_Ore();
	private int fuelMinimumUse = 50;
	private int fuelDuration = 100;
	
	private String cCastlessName = null;
	private String cCoinName = null;
	private String cTinyName = null;
	private String cDustName = null;
	private String cGemName = null;
	private String cNuggetName = null;
	private String cCrystalName = null;
	private String cCrystalShardName = null;
	private String cClusterName = null;
	private String cIngotName = null;
	private String cRodName = null;
	private String cPlateName = null;
	private String cDensePlateName = null;
	private String cBlockName = null;
	private String cOreName = null;
	private String cCrushedName = null;
	private String cPurifiedName = null;

	private boolean isFuel = false;
	public OreDictSmelterIntegration(String oreDictName, Fluid fluid) {
		this.oreDictName = oreDictName;
		this.fluid = fluid;
	}

	public OreDictSmelterIntegration setBackfillOnly() {
		this.backfillOnly = true;
		return this;
	}
	
	public OreDictSmelterIntegration setCastless(String oreDictionaryName) {
		this.cCastlessName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setCoin(String oreDictionaryName) {
		this.cCoinName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setTiny(String oreDictionaryName) {
		this.cTinyName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setDust(String oreDictionaryName) {
		this.cDustName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setGem(String oreDictionaryName) {
		this.cGemName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setNugget(String oreDictionaryName) {
		this.cNuggetName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setCrystal(String oreDictionaryName) {
		this.cCrystalName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setCrystalShard(String oreDictionaryName) {
		this.cCrystalShardName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setCluster(String oreDictionaryName) {
		this.cClusterName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setIngot(String oreDictionaryName) {
		this.cIngotName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setRod(String oreDictionaryName) {
		this.cRodName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setPlate(String oreDictionaryName) {
		this.cPlateName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setDensePlate(String oreDictionaryName) {
		this.cDensePlateName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setBlock(String oreDictionaryName) {
		this.cBlockName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setOre(String oreDictionaryName) {
		this.cOreName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setCrushed(String oreDictionaryName) {
		this.cCrushedName = oreDictionaryName;
		return this;
	}
	
	public OreDictSmelterIntegration setPurified(String oreDictionaryName) {
		this.cPurifiedName = oreDictionaryName;
		return this;
	}

	public OreDictSmelterIntegration setValCastless(int val) {
		this.valCastless = val;
		return this;
	}

	public OreDictSmelterIntegration setValCoin(int val) {
		this.valCoin = val;
		return this;
	}

	public OreDictSmelterIntegration setValTiny(int val) {
		this.valTiny = val;
		return this;
	}

	public OreDictSmelterIntegration setValDust(int val) {
		this.valDust = val;
		return this;
	}
	
	public OreDictSmelterIntegration setValGem(int val) {
		this.valGem = val;
		return this;
	}
	
	public OreDictSmelterIntegration setValOre(int val) {
		this.valOre = val;
		this.valCrushed = val;
		this.valPurified = val;
		return this;
	}
	
	public OreDictSmelterIntegration setValNugget(int val) {
		this.valNugget = val;
		return this;
	}
	
	public OreDictSmelterIntegration setValCrystal(int val) {
		this.valCrystal = val;
		return this;
	}
	
	public OreDictSmelterIntegration setValCrystalShard(int val) {
		this.valCrystalShard = val;
		return this;
	}
	
	public OreDictSmelterIntegration setValCluster(int val) {
		this.valCluster = val;
		return this;
	}
	
	public OreDictSmelterIntegration setValIngot(int val) {
		this.valIngot = val;
		return this;
	}
	
	public OreDictSmelterIntegration setValRod(int val) {
		this.valRod = val;
		return this;
	}
	
	public OreDictSmelterIntegration setValPlate(int val) {
		this.valPlate = val;
		return this;
	}
	
	public OreDictSmelterIntegration setValDensePlate(int val) {
		this.valDensePlate = val;
		return this;
	}
	
	public OreDictSmelterIntegration setValBlock(int val) {
		this.valBlock = val;
		return this;
	}

	public OreDictSmelterIntegration setFuel(int minimumUse, int duration) {
		this.fuelMinimumUse = minimumUse;
		this.fuelDuration = duration;
		this.isFuel = true;
		return this;
	}
	
	private static void registerMeltingCollection(NonNullList<ItemStack> collection, String name, Fluid fluid, int val) {
		if (collection.size() > 0) {
			for (int i = 0; i < collection.size(); i++) {
			    ItemStack thisItem = OreDictionary.getOres(name).get(i);
				TinkerRegistry.registerMelting(
						thisItem,
						fluid,
						val
				);
			}

			
			if (Loader.isModLoaded("thermalexpansion")) {
				ItemStack thisItem = OreDictionary.getOres(name).get(OreDictionary.getOres(name).size()-1);
				CrucibleManager.addRecipe(CrucibleManager.DEFAULT_ENERGY / 4, thisItem, new FluidStack(fluid, val));
			}
		}
	}
	
	public OreDictSmelterIntegration melt(String name, Integer value) {
		NonNullList<ItemStack> collection = OreDictionary.getOres(name);
		if (collection.size() > 0) {
			for (int i = 0; i < collection.size(); i++) {
			    ItemStack thisItem = OreDictionary.getOres(name).get(i);
				TinkerRegistry.registerMelting(
						thisItem,
						this.fluid,
						value
				);
			}

			if (Loader.isModLoaded("thermalexpansion")) {
				ItemStack thisItem = OreDictionary.getOres(name).get(OreDictionary.getOres(name).size()-1);
				CrucibleManager.addRecipe(CrucibleManager.DEFAULT_ENERGY / 4, thisItem, new FluidStack(fluid, value));
			}
		}
		
		return this;
	}
	
	private static void registerTableCastingCollection(NonNullList<ItemStack> collection, ItemStack cast, Fluid fluid, int val) {
		if (collection.size() > 0) {
			ICastingRecipe castRecipe = TinkerRegistry.getTableCasting(collection.get(collection.size()-1), fluid);
			if (castRecipe == null) {
				TinkerRegistry.registerTableCasting(
						collection.get(collection.size()-1),
						cast,
						fluid,
						val
				);
			}
		}
	}
	
	private static void registerBasinCastingCollection(NonNullList<ItemStack> collection, ItemStack cast, Fluid fluid, int val) {
		if (collection.size() > 0) {
			TinkerRegistry.registerBasinCasting(
					collection.get(collection.size()-1),
					cast,
					fluid,
					val
			);
		}
	}


	public static CrusherRecipe addCrusherRecipe(ItemStack output, Object input, int energy, Object... secondary) {
		CrusherRecipe r = CrusherRecipe.addRecipe(output, input, energy);
		if(secondary!=null&&secondary.length > 0)
			r.addToSecondaryOutput(secondary);
		return r;
	}
	
	private static void registerCrusherRecipes(NonNullList<ItemStack> outputCollection, String inputName, int amount) {
		//addCrusherRecipe(new ItemStack(Items.QUARTZ, 4), "blockQuartz", 3200);
		//addCrusherRecipe();
		ItemStack itemstack = outputCollection.get(outputCollection.size()-1);
		addCrusherRecipe(Utils.copyStackWithAmount(itemstack, amount), inputName, 3200);
	}

	public void register() {
		// get our tinker casts
		Item normalCast = Item.REGISTRY.getObject(new ResourceLocation("tconstruct:cast"));
		Item customCast = Item.REGISTRY.getObject(new ResourceLocation("tconstruct:cast_custom"));

		/* rod cast not working the way we want atm
		ItemStack castRod = new ItemStack(normalCast, 1, 0);
		NBTTagCompound tag = new NBTTagCompound();
    	tag.setString("PartType", "tconstruct:tool_rod");
    	castRod.writeToNBT(tag);
    	*/

		//castRod.stackTagCompound.setString("partType", "")
		ItemStack castIngot = new ItemStack(customCast, 1, 0);
		ItemStack castNugget = new ItemStack(customCast, 1, 1);
		ItemStack castGem = new ItemStack(customCast, 1, 2);
		ItemStack castPlate = new ItemStack(customCast, 1, 3);
		ItemStack castDust = new ItemStack(customCast, 1, 4);

		// get our ore dictionary names
		String dCoin = cCoinName != null ? cCoinName : "coin" + oreDictName;
		String dTiny = cTinyName != null ? cTinyName : "dustTiny" + oreDictName;
		String dDust = cDustName != null ? cDustName : "dust" + oreDictName;
		String dCrushed = cCrushedName != null ? cCrushedName : "crushed" + oreDictName;
		String dPurified = cPurifiedName != null ? cPurifiedName : "purified" + oreDictName;
		String dPlate = cPlateName != null ? cPlateName : "plate" + oreDictName;
		String dDensePlate = cDensePlateName != null ? cDensePlateName : "plateDense" + oreDictName;
		String dGem = cGemName != null ? cGemName : "gem" + oreDictName;
		String dOre = cOreName != null ? cOreName : "ore" + oreDictName;
		String dNugget = cNuggetName != null ? cNuggetName : "nugget" + oreDictName;
		String dCrystal = cCrystalName != null ? cCrystalName : "crystal" + oreDictName;
		String dCrystalShard = cCrystalShardName != null ? cCrystalShardName : "crystalShard" + oreDictName;
		String dCluster = cClusterName != null ? cClusterName : "cluster" + oreDictName;
		String dIngot = cIngotName != null ? cIngotName : "ingot" + oreDictName;
		String dRod = cRodName != null ? cRodName : "rod" + oreDictName;
		String dBlock = cBlockName != null ? cBlockName : "block" + oreDictName;
		String dGear = "gear" + oreDictName;
		String dWire = "wire" + oreDictName;
		

		// get our ore dictionary collections
		NonNullList<ItemStack> coinCollection = OreDictionary.getOres(dCoin);
		NonNullList<ItemStack> tinyCollection = OreDictionary.getOres(dTiny);
		NonNullList<ItemStack> dustCollection = OreDictionary.getOres(dDust);
		NonNullList<ItemStack> crushedCollection = OreDictionary.getOres(dCrushed);
		NonNullList<ItemStack> purifiedCollection = OreDictionary.getOres(dPurified);
		NonNullList<ItemStack> clusterCollection = OreDictionary.getOres(dCluster);
		NonNullList<ItemStack> plateCollection = OreDictionary.getOres(dPlate);
		NonNullList<ItemStack> densePlateCollection = OreDictionary.getOres(dDensePlate);
		NonNullList<ItemStack> gemCollection = OreDictionary.getOres(dGem);
		NonNullList<ItemStack> oreCollection = OreDictionary.getOres(dOre);
		NonNullList<ItemStack> nuggetCollection = OreDictionary.getOres(dNugget);
		NonNullList<ItemStack> crystalCollection = OreDictionary.getOres(dCrystal);
		NonNullList<ItemStack> crystalShardCollection = OreDictionary.getOres(dCrystalShard);
		NonNullList<ItemStack> ingotCollection = OreDictionary.getOres(dIngot);
		NonNullList<ItemStack> rodCollection = OreDictionary.getOres(dRod);
		NonNullList<ItemStack> blockCollection = OreDictionary.getOres(dBlock);
		NonNullList<ItemStack> gearCollection = OreDictionary.getOres(dGear);
		NonNullList<ItemStack> wireCollection = OreDictionary.getOres(dWire);

		if (backfillOnly) {
			Main.logger.info("Backfilling extra support for " + oreDictName);
			
			// registerTableCastingCollection(dustCollection, castDust, fluidMolten, valDust);
			registerMeltingCollection(coinCollection, dCoin, fluid, valCoin);
			registerMeltingCollection(tinyCollection, dTiny, fluid, valTiny);
			registerMeltingCollection(dustCollection, dDust, fluid, valDust);
			registerMeltingCollection(crushedCollection, dCrushed, fluid, valCrushed);
			registerMeltingCollection(purifiedCollection, dPurified, fluid, valPurified);
			registerMeltingCollection(crystalCollection, dCrystal, fluid, valCrystal);
			registerMeltingCollection(crystalShardCollection, dCrystalShard, fluid, valCrystalShard);
			registerMeltingCollection(clusterCollection, dCluster, fluid, valCluster);
			registerMeltingCollection(plateCollection, dPlate, fluid, valPlate);
			registerMeltingCollection(densePlateCollection, dDensePlate, fluid, valDensePlate);
			registerTableCastingCollection(plateCollection, castPlate, fluid, valPlate);
		} else {
			Main.logger.info("Integrating " + oreDictName + " with the smeltery");			
			// register melting
			registerMeltingCollection(coinCollection, dCoin, fluid, valCoin);
			registerMeltingCollection(tinyCollection, dTiny, fluid, valTiny);
			registerMeltingCollection(dustCollection, dDust, fluid, valDust);
			registerMeltingCollection(gemCollection, dGem, fluid, valGem);
			registerMeltingCollection(oreCollection, dOre, fluid, valOre);
			registerMeltingCollection(crushedCollection, dCrushed, fluid, valCrushed);
			registerMeltingCollection(purifiedCollection, dPurified, fluid, valPurified);
			registerMeltingCollection(nuggetCollection, dNugget, fluid, valNugget);
			registerMeltingCollection(crystalCollection, dCrystal, fluid, valCrystal);
			registerMeltingCollection(crystalShardCollection, dCrystalShard, fluid, valCrystalShard);
			registerMeltingCollection(ingotCollection, dIngot, fluid, valIngot);
			registerMeltingCollection(rodCollection, dRod, fluid, valRod);
			registerMeltingCollection(clusterCollection, dCluster, fluid, valCluster);
			registerMeltingCollection(plateCollection, dPlate, fluid, valPlate);
			registerMeltingCollection(densePlateCollection, dDensePlate, fluid, valDensePlate);
			registerMeltingCollection(blockCollection, dBlock, fluid, valBlock);
			
			// register table casting
			// registerTableCastingCollection(dustCollection, castDust, fluid, valDust);
			registerTableCastingCollection(gemCollection, castGem, fluid, valGem);
			registerTableCastingCollection(nuggetCollection, castNugget, fluid, valNugget);
			registerTableCastingCollection(ingotCollection, castIngot, fluid, valIngot);
			// registerTableCastingCollection(rodCollection, castRod, fluid, valRod);
			registerTableCastingCollection(plateCollection, castPlate, fluid, valPlate);
			
			// register basin casting
			registerBasinCastingCollection(blockCollection, ItemStack.EMPTY, fluid, valBlock);
			
			// register immersive engineering crusher
			if (Loader.isModLoaded("immersiveengineering") && dustCollection.size() > 0) {
				if (oreCollection.size() > 0) {
					//crush ores
					registerCrusherRecipes(dustCollection, dOre, 2);
				}
				
				if (ingotCollection.size() > 0) {
					//crush ingots
					registerCrusherRecipes(dustCollection, dIngot, 1);
				}
			}
			
			// register immersive engineering press for stick, gear, plate
			if (Loader.isModLoaded("immersiveengineering") && ingotCollection.size() > 0) {
				ComparableItemStack compMoldPlate = ApiUtils.createComparableItemStack(new ItemStack(IEContent.itemMold, 1, 0), false);
				ComparableItemStack compMoldGear = ApiUtils.createComparableItemStack(new ItemStack(IEContent.itemMold, 1, 1), false);
				ComparableItemStack compMoldRod = ApiUtils.createComparableItemStack(new ItemStack(IEContent.itemMold, 1, 2), false);
				ComparableItemStack compMoldWire = ApiUtils.createComparableItemStack(new ItemStack(IEContent.itemMold, 1, 4), false);

				if (rodCollection.size() > 0) {
					MetalPressRecipe.addRecipe(rodCollection.get(rodCollection.size() - 1), dIngot, compMoldRod, 2400);
				}
				if (gearCollection.size() > 0) {
					MetalPressRecipe.addRecipe(gearCollection.get(gearCollection.size() - 1), dIngot, compMoldGear, 2400).setInputSize(4);
				}
				
				if (plateCollection.size() > 0) {
					MetalPressRecipe.addRecipe(plateCollection.get(plateCollection.size() - 1), dIngot, compMoldPlate, 2400);
				}
				
				if (wireCollection.size() > 0) {
					MetalPressRecipe.addRecipe(wireCollection.get(wireCollection.size() - 1), dWire, compMoldWire, 2400);
				}
			}
		}
		
		if (cCastlessName != null) {
			String dCastless = cCastlessName;
			NonNullList<ItemStack> castlessCollection = OreDictionary.getOres(dCastless);
			registerMeltingCollection(castlessCollection, dCastless, fluid, valCastless);
			registerTableCastingCollection(castlessCollection, ItemStack.EMPTY, fluid, valCastless);
		}
		
		// extra meltables
		
		
		// register fuel
		if (isFuel) {
			Main.logger.info("Registering Fuel: " + oreDictName);
			TinkerRegistry.registerSmelteryFuel(new FluidStack(fluid, fuelMinimumUse), fuelDuration);
		}
	}
}
