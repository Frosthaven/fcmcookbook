package com.fortiscoregaming.minecraftcookbook;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistryModifiable;
import slimeknights.tconstruct.library.TinkerRegistry;
import slimeknights.tconstruct.library.materials.Material;
import slimeknights.tconstruct.shared.TinkerFluids;

import java.util.ArrayList;
import java.util.logging.LogManager;

import org.apache.logging.log4j.Logger;

import com.fortiscoregaming.minecraftcookbook.fluid.CustomFluids;
import com.fortiscoregaming.minecraftcookbook.tconstruct.InitSmeltery;
import com.fortiscoregaming.minecraftcookbook.tconstruct.lib.OreDictSmelterIntegration;
/* @todo
 *  > BIOMES O PLENTY GEMS
 *  > BOTANIA INGOTS
 *  > NEW TEXTURES FOR GEM LIQUIDS AND MAGIC INGOTS?
 *  > FIGURE OUT IF WE CAN USE OREDICT WILDCARDS INSTEAD OF MAKING EVERY SINGLE ENTRY A RECIPE
 */

@Mod(modid = Main.MODID, name = Main.NAME, version = Main.VERSION, dependencies = "after:opencomputers;after:opensecurity;after:evilcraft;after:quark;after:xnet;after:hammercore;after:draconicevolution;after:astralsorcery;after:galacticraftplanets;after:galacticraftcore; after:icbmclassic;after:abyssalcraft;after:tconstruct;after:ic2;after:immersiveengineering;after:immersivepetroleum;after:redstoneflux;after:thermalfoundation;after:thermalexpansion;after:thaumcraft;after:cofhcore;after:cofhworld")
public class Main
{
    public static final String MODID = "fcmcookbook";
    public static final String NAME = "Fortis Core Cookbook";
    public static final String VERSION = "1.12.2-3.7";

    public static Logger logger;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        logger = event.getModLog();
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        //initialized!
        logger.info(Main.NAME + " v" + Main.VERSION + " initialized!");
        CustomFluids.registerFluids();
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    	if (Loader.isModLoaded("tconstruct")) {
    		InitSmeltery.init();
    	}
    	/*
    	String[] list = OreDictionary.getOreNames();
    	for (int i = 0; i < list.length; i++) {
    		Main.logger.info(list[i]);
    	}
    	*/
    }
}
