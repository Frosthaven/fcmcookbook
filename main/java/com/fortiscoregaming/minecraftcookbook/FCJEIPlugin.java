package com.fortiscoregaming.minecraftcookbook;

import java.util.ArrayList;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableSet;

import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.ISubtypeRegistry;
import mezz.jei.api.ingredients.IIngredientBlacklist;
import mezz.jei.api.ingredients.IIngredientRegistry;
import mezz.jei.api.IJeiRuntime;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.JEIPlugin;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.oredict.OreDictionary;

@JEIPlugin
public class FCJEIPlugin implements IModPlugin {
	@Nullable
	private ISubtypeRegistry subtypeRegistry;

	public static ItemStack fetchItem(String resourceName, int meta) {
		Item entry = Item.REGISTRY.getObject(new ResourceLocation(resourceName));
		return new ItemStack(entry, 1, meta);
	}

	public static ItemStack fetchBlock(String resourceName, int meta) {
		Block entry = Block.REGISTRY.getObject(new ResourceLocation(resourceName));
		return new ItemStack(entry, 1, meta);
	}

	@Override
	public void register(IModRegistry registry) {
		IIngredientRegistry ingredientRegistry = registry.getIngredientRegistry();
		IJeiHelpers jeiHelpers = registry.getJeiHelpers();
		
		IIngredientBlacklist ingredientBlacklist = registry.getJeiHelpers().getIngredientBlacklist();
		
		if (Loader.isModLoaded("thermalexpansion")) {
			ImmutableSet<Integer> machineList = ImmutableSet.of(0, 1, 5, 7, 9, 11, 12, 13, 15);
			ImmutableSet<Integer> deviceList = ImmutableSet.of(3, 4, 5, 6, 10, 12);
			
			for (final Integer id : machineList) {
				ingredientBlacklist.addIngredientToBlacklist(fetchBlock("thermalexpansion:machine",id));
			}
			
			for (final Integer id : deviceList) {
				ingredientBlacklist.addIngredientToBlacklist(fetchBlock("thermalexpansion:device",id));
			}
			
			ingredientBlacklist.addIngredientToBlacklist(fetchBlock("thermalexpansion:cache",0));
		}
	}
}
